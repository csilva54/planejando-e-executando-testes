﻿using System;

namespace UcPag
{   
    public class Pagamento
    {
        public Pagamento() { }

        public double GerarTaxaJuro()
        {          
            return 0.01;
        }

        public double CalculaJuros(double valorinicial, int periodo)
        {
            if (valorinicial > 0 && periodo > 0)
            {
                var valorFinal = valorinicial * Math.Pow((1 + GerarTaxaJuro()), periodo);                
                return Math.Round(valorFinal, 2);
                
            }
            else
            {
                throw new ArgumentOutOfRangeException("Valores com zero!");
            }            
        }
        
        public static void Main()
        {

            Pagamento pagamento = new Pagamento();
            var Juro = pagamento.CalculaJuros(7, 5);

            Console.WriteLine(Juro);
            Console.ReadKey();
        }
    }
}