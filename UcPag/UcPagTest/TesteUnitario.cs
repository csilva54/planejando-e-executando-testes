using Microsoft.VisualStudio.TestTools.UnitTesting;
using UcPag;

namespace UcPagTest
{

    [TestClass]
    public class TesteUnitario
    {
        [TestMethod]
        public void Teste_taxa_retorno_sucesso()
        {
            double esperado = 0.01;

            Pagamento pagamento = new Pagamento();
            double atual = pagamento.GerarTaxaJuro();
                        
            Assert.AreEqual(esperado, atual, 0.001, "Teste RF01 retorno 1% or 0.01");
        }

        [TestMethod]
        public void teste_calculo()
        {
            Pagamento pagamento = new Pagamento();
            double inicial = 0;
            int periodo = 2;         

            Assert.ThrowsException<System.ArgumentOutOfRangeException>(() => pagamento.CalculaJuros(inicial, periodo));
        }

        [TestMethod]
        public void teste_Valorfinal()
        {

            Pagamento pagamento = new Pagamento();
            double esperado = 6.12;
            double inicial = 6;
            int periodo = 2;

            var retorno = pagamento.CalculaJuros(inicial, periodo);

            Assert.AreEqual(esperado, retorno, 0.001, "Funciona");
        }
    }
}
